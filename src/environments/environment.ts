// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBclst_kZ_TN4R3_bI9A4tr7W8rNWwaUj4",
    authDomain: "platzinger-5450e.firebaseapp.com",
    databaseURL: "https://platzinger-5450e.firebaseio.com",
    projectId: "platzinger-5450e",
    storageBucket: "gs://platzinger-5450e.appspot.com/",
    messagingSenderId: "888296080215",
    appId: "1:888296080215:web:d0d4046b2829d0db"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
