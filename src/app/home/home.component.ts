import { Component, OnInit } from '@angular/core';
import { User } from '../interfaces/user';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { RequestsService } from '../services/requests.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  friends: User[];
  query: string;
  friendEmail: string = '';
  user: User;
  avatar: string;
  closeResult: string;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
    private modalService: NgbModal,
    private requestService: RequestsService,
    private angularFireAuth: AngularFireAuth) {

    this.authService.getStatus().subscribe(
      (status) => {
        console.log(status);
        this.userService.getUserById(status.uid).valueChanges().subscribe(
          (data: User) => {
            console.log(data);
            this.user = data;

            if (this.user.friends) {
              this.user.friends = Object.values(this.user.friends);
              console.log(this.user);

            }
          },
          (error) => {
            console.log(error);

          }
        );
      },
      (error) => {

      }
    );

    // Usar un subscribe para obtener todos los usuarios desde Firebase...
    this.userService.getUsers().valueChanges().subscribe(
      (data: User[]) => {
        this.friends = data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  ngOnInit() { }

  logout() {
    this.authService.logOut()
      .then(() => {
        alert('cerrando sesion...');
        this.router.navigate(['login']);
      }).catch((error) => {
        console.log(error);
      })
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  sendRequest() {
    const request = {
      timestamp: Date.now(),
      receiver_email: this.friendEmail,
      sender: this.user.uid,
      status: 'pending'
    }

    this.requestService.createRequest(request)
      .then(() => {
        alert("Solicitud enviada");
      })
      .catch((error) => {
        alert("Ha ocurrido un error!");
        console.log(error);
      });
  }

}
