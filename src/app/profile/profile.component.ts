import { Component, OnInit } from '@angular/core';
import { User } from '../interfaces/user';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { FirebaseStorage } from '@angular/fire';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  user: User;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  picture: any;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private firebaseStorage: AngularFireStorage) {
    this.authService.getStatus().subscribe(
      (status) => {
        this.userService.getUserById(status.uid).valueChanges().subscribe(
          (data: User) => {
            this.user = data;
            console.log(this.user);
          },
          (error) => {
            console.log(error);
          },
        );
      },
      (error) => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
  }

  saveSettings() {

    if (this.croppedImage) { // Verificamos si tenemos una imagen recortada...

      const currentPictureId = Date.now(); // Creamos un nombre unico para la imagen...
      const pictures = this.firebaseStorage // Referenciamos pictures dentro de nuestro firebaseStorage
        .ref('pictures/' + currentPictureId + '.jpg')
        .putString(this.croppedImage, 'data_url');

      pictures.then((result) => {

        // Obtención de la URL hacia ese archivo binario...
        this.picture = this.firebaseStorage.ref('pictures/' + currentPictureId + '.jpg').getDownloadURL();
        this.picture.subscribe(
          (p) => {
            this.userService.setAvatar(p, this.user.uid)
              .then(
                () => {
                  alert("AVATAR ALMACENADO CON ÉXITO!...");
                  console.log("AVATAR ALMACENADO CON ÉXITO!...");
                }
              ).catch(
                (error) => {
                  alert("Ocurrio un error al subir la imagen!");
                  console.log("Ocurrio un error al subir la imagen: ", error);
                }
              )
          }
        )
      }).catch((error) => {
        console.log(error);
      });

    } else {
      this.userService.editUser(this.user)
        .then(
          (data) => {
            console.log('Datos Guardados!...');
            console.log(data);
          }
        ).catch(
          (error) => {
            console.log(error);
          }
        );
    }

  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
}
