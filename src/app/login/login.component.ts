import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  operation: string = 'login';
  email: string = null;
  pass: string = null;
  nick: string = null;

  constructor(private authService: AuthService, private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  login() {
    this.authService.loginWithEmail(this.email, this.pass)
      .then((data) => {
        alert("LOGUEADO CORRECTAMENTE!...");
        console.log(data);

        this.router.navigate(['home']);
      }).catch((error) => {
        alert("Oopss.. Ocurrio un error =(...");
        console.log(error);
      })
  }

  register() {
    this.authService.registerWithEmail(this.email, this.pass)
      .then((data) => {
        const user = {
          uid: data.user.uid,
          email: this.email,
          nick: this.nick
        };
        this.userService.createUser(user).
          then((data2) => {
            alert("REGISTRADO CORRECTAMENTE!...");
            console.log(data2);
          }).
          catch((error2) => {
            alert("Oopss.. Ocurrio un error =(...");
            console.log(error2);
          });

      }).catch((error) => {
        alert("Oopss.. Ocurrio un error =(...");
        console.log(error);
      })
  }
}
