import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../interfaces/user';
import { UserService } from '../services/user.service';
import { ConversationService } from '../services/conversation.service';
import { AuthService } from '../services/auth.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AngularFireStorage } from '@angular/fire/storage';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.scss']
})
export class ConversationComponent implements OnInit, AfterViewInit {

  friendId: any;
  friends: User[];
  friend: User;
  user: User;
  conversationId: string;
  textMessage: string;
  conversation: any;
  shake: boolean = false;
  croppedImage: any = '';
  picture: any = '';
  imageChangedEvent: any = '';
  closeResult: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private conversationService: ConversationService,
    private authService: AuthService,
    private firebaseStorage: AngularFireStorage,
    private modalService: NgbModal
  ) {
    this.friendId = this.activatedRoute.snapshot.params['uid'];

    this.authService.getStatus().subscribe(
      (session) => {
        this.userService.getUserById(session.uid).valueChanges().subscribe(
          (user: User) => {
            this.user = user;

            // Usar un subscribe para obtener el usuario especifico desde Firebase...
            this.userService.getUserById(this.friendId).valueChanges().subscribe(
              (data: User) => {
                this.friend = data;

                const ids = [this.user.uid, this.friend.uid].sort();
                this.conversationId = ids.join('|');

                this.getConversation();
              },
              (error) => {
                console.log(error);
              }
            );
          }
        )
      }
    );
  }

  ngOnInit() { }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngAfterViewInit() {
  }

  sendMessage() {
    const message = {
      uid: this.conversationId,
      timestamp: Date.now(),
      text: this.textMessage,
      sender: this.user.uid,
      receiver: this.friend.uid,
      type: 'text'
    }

    this.conversationService.createConversation(message).then(
      () => {
        this.textMessage = '';
      }
    );
  }

  sendImage() {

    const currentPictureId = Date.now(); // Creamos un nombre unico para la imagen...
    const pictures = this.firebaseStorage.ref('pictures/' + currentPictureId + '.jpg')
      .putString(this.croppedImage, 'data_url');

    pictures.then(() => {

      // Obtención de la URL hacia ese archivo binario...
      this.picture = this.firebaseStorage.ref('pictures/' + currentPictureId + '.jpg').getDownloadURL();
      this.picture.subscribe(
        (p) => {
          const message = {
            uid: this.conversationId,
            timestamp: Date.now(),
            text: p,
            sender: this.user.uid,
            receiver: this.friend.uid,
            type: 'image'
          }

          this.conversationService.createConversation(message)
            .then(() => {
              this.croppedImage = '';
            });
        }
      )
    }).catch((error) => {
      console.log(error);
    });
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  sendZumbido() {
    const message = {
      uid: this.conversationId,
      timestamp: Date.now(),
      text: null,
      sender: this.user.uid,
      receiver: this.friend.uid,
      type: 'zumbido'
    }

    this.conversationService.createConversation(message).then(() => { });
    this.doZumbido(message);
  }

  doZumbido(messageIn) {
    if (this.user.uid === messageIn.receiver) {
      const audio = new Audio('assets/sound/zumbido.m4a');
      audio.play();
      this.shake = true;

      window.setTimeout(() => {
        this.shake = false;
      }, 1000);
    }
  }
  doNewMessage(messageIn) {
    if (this.user.uid === messageIn.receiver) {
      const audio = new Audio('assets/sound/new_message.m4a');
      audio.play();
    }
  }

  getConversation() {

    console.log(this.user);


    this.conversationService.getConversation(this.conversationId).valueChanges().subscribe(
      (data) => {
        console.log(data);
        this.conversation = data;
        this.conversation.forEach((message) => {
          if (!message.seen) {
            message.seen = true;
            this.conversationService.editConversation(message);

            if (message.type == 'text') {
              this.doNewMessage(message);
            } else if (message.type == 'image') {
              this.doNewMessage(message);
            } else if (message.type == 'zumbido') {
              this.doZumbido(message);
            }

          }
        });
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getUserNickById(id) {
    if (id === this.friend.uid) {
      return this.friend.nick;
    } else {
      return this.user.nick;
    }
  }
}
