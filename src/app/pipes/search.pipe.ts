import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value) { // Si no hay amigos
      return;
    }

    if (!args) { // Si no especifique algun amigo, regresame todos los amigos
      return value;
    }

    args = args.toLowerCase(); // Convierto a minusculas el texto ingresado en el buscador a minúsculas

    return value.filter((item) => {
      return JSON.stringify(item)
        .toLowerCase()
        .includes(args); // Filtro de algun amigo dentro del listado segun el texto indicado en el buscador
    });
  }

}
