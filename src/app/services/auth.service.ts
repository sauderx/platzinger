import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private angularFireAuth: AngularFireAuth) { }

  loginWithEmail(email:string, pass: string) {
    return this.angularFireAuth.auth.signInWithEmailAndPassword(email, pass);
  }

  registerWithEmail(email:string, pass: string) {
    return this.angularFireAuth.auth.createUserWithEmailAndPassword(email, pass);
  }

  getStatus() {
    return this.angularFireAuth.authState;
  }

  logOut() {
    return this.angularFireAuth.auth.signOut();
  }
}
