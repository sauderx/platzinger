import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { RequestsService } from './services/requests.service';
import { User } from './interfaces/user';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { RequestComponent } from './modals/request/request.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'platzinger';
  user: User;
  requests: any[] = [];
  mailsShown: any[] = [];

  constructor(
    public router: Router,
    private authService: AuthService,
    private userService: UserService,
    private requestService: RequestsService,
    private dialogService: DialogService) {

    this.authService.getStatus().subscribe(
      (status) => {
        if (status) {
          console.log(status);
          // Obtengo la info de mi usuario actual logueado...
          this.userService.getUserById(status.uid).valueChanges().subscribe(
            (data: User) => {
              this.user = data;
              console.log(data);

              // Obtengo todas las solicitudes de amistad enviadas al usuario actual...
              this.requestService.getRequestForEmail(this.user.email).valueChanges().subscribe(
                (requests: any) => {
                  this.requests = requests; // Almaceno todas las solicitudes de manera global...
                  console.log(this.requests);

                  // Filtro las solicitudes válidas....
                  this.requests = this.requests.filter((r) => {
                    return r.status !== 'accepted' && r.status !== 'rejected';
                  })

                  this.requests.forEach((req) => {
                    if (this.mailsShown.indexOf(req.sender) === -1) {
                      this.mailsShown.push((req.sender));
                      this.dialogService.addDialog(RequestComponent, { scope: this, currentRequest: req });
                    }
                  })
                },
                (error) => {
                  console.log(error);
                }
              );
            }
          );

        }


      }
    )

  }
}
